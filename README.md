# NEW INSTRUCTIONS
1. SSH to amazon aws instance using following command: ssh -i "distrotech-demo.pem" ubuntu@ec2-54-165-181-165.compute-1.amazonaws.com
2. Clone this repository - git clone https://hareesh_sukshi@bitbucket.org/hareesh_sukshi/model_training_gce.git
3. Install requirements: pip3 install -r requirements.txt
4. Run the final command - python3 run_on_server.py
   - Input Status Code
   - Input csv file path
   ----- Output File will be generated

# How to copy csv file to Amazon Instance
Command -  scp -i distrotech-demo.pem <local-csv-file-path-whichi-you-want-to-copy> ubuntu@ec2-54-165-181-165.compute-1.amazonaws.com:<path-on-instance-where-you-want-to-place-the-file>
# How to copy csv file From Amazon Instance
Command : scp -i distrotech-demo.pem ubuntu@ec2-54-165-181-165.compute-1.amazonaws.com:<path-on-instance-which-you-want-to-download-locally> <local-path-where-you-want-to-copy-the-file>








# OLD INSTRUCTIONS
1. Create a compute engine.
2. Install python 3.6.5
	sudo apt-get install build-essential checkinstall
	sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
	cd ~/Downloads/
	wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
	tar -xvf Python-3.6.5.tgz
	cd Python-3.6.5
	./configure
	make
	sudo checkinstall
3. Install pip
4. Run the following command, pip3 install -r requirements.txt
5. Final Command : python3 run_on_server.py
6. Output CSV file will be generated