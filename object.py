from imageai.Detection import ObjectDetection
import os
import time
import cv2
import dlib
import csv
import math
import imutils
import argparse
import subprocess

execution_path=os.getcwd()
temp_folder_path = os.path.join(execution_path,'temp')

def midpointperson(img_path):
	frame=cv2.imread(img_path)
	try:
		detector1=dlib.get_frontal_face_detector()
		if len(frame)>0:
			gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
			detected_faces=detector1(gray,0)
			height = frame.shape[0]
			width = frame.shape[1]

			face_rect=detected_faces[0]

			a=face_rect.left()
			b=face_rect.top()
			c=face_rect.right()
			d=face_rect.bottom()

			if(face_rect.left()<0):
				a=0
			if(face_rect.top()<0):
				b=0
			if(face_rect.right()>width):
				c=width-1
			if(face_rect.bottom()>height):
				d=height-1
			#left_top=(a,b)
			#right_bottom=(c,d)
			x=(a+c)/2
			y=(b+d)/2

			mid_point=(x,y)
			#print(mid_point)
			return mid_point
	except:
		pass


def findlapres(box_lap,mid_point_person):
	if(mid_point_person[0]>box_lap[0] and mid_point_person[0]<box_lap[2] and mid_point_person[1]>box_lap[1] and mid_point_person[1]<box_lap[3]):
		return "fake"
	else:
		return "real"

def findtvres(box_tv,mid_point_person):
	if(mid_point_person[0]>box_tv[0] and mid_point_person[0]<box_tv[2] and mid_point_person[1]>box_tv[1] and mid_point_person[1]<box_tv[3]):
		return "fake"
	else:
		return "real"
def findcellres(box_cell,mid_point_person):
	if(mid_point_person[0]>box_cell[0] and mid_point_person[0]<box_cell[2] and mid_point_person[1]>box_cell[1] and mid_point_person[1]<box_cell[3]):
		return "fake"
	else:
		return "real"

ap=argparse.ArgumentParser()
ap.add_argument("-f","--folder",type=str,default="",help="path to folder containing images")
args=vars(ap.parse_args())

img=args["folder"]

image=os.listdir(img)

detector=ObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath("yolo.h5")
load_time=time.time()
detector.loadModel()
end_time=time.time()
total_time=end_time-load_time
print(total_time)

for images in image:
	detect_time=time.time()
	img_path=os.path.join(img,images)
	if (images.endswith(".png") or images.endswith(".jpg") or images.endswith(".jpeg")):
		print(images)
		try:
			custom=detector.CustomObjects(person=True,cell_phone=True,tv=True,laptop=True)
			detections=detector.detectCustomObjectsFromImage(custom_objects=custom,input_image=img_path,output_image_path=os.path.join(temp_folder_path,"new1"+images),minimum_percentage_probability=30)
			tv_detected= False
			lap_detected= False
			cell_detected= False
			tvres="null"
			lapres="nul"
			cellres="null"
			final_result="real"
			for eachObject in detections:
				if(eachObject["name"]=="tv"):
					tv_detected= True
					#print(eachObject["name"])
					box_tv=eachObject["box_points"]
					#print(box_tv)

				if(eachObject["name"]=="cell phone"):
					cell_detected= True
					#print(eachObject["name"])
					box_cell=eachObject["box_points"]
					#print(box_cell)

				#if(eachObject["name"]=="person"):
					#print(eachObject["name"])
					# print(eachObject["box_points"])


				if(eachObject["name"]=="laptop"):
					lap_detected= True
					#print(eachObject["name"])
					box_lap=eachObject["box_points"]
					#print(box_lap)
			mid_point_person=midpointperson(img_path)
			

			if(lap_detected==True):
				lapres=findlapres(box_lap,mid_point_person)
				# print(lapres)
			if(tv_detected==True):
				tvres=findtvres(box_tv,mid_point_person)
				# print(tvres)
			if(cell_detected==True):
				cellres=findcellres(box_cell,mid_point_person)
				# print(box_cell)
			if(lap_detected==False and tv_detected==False and cell_detected==False):
				final_result="No Object Found"
				print("real")
			if(tvres=="fake" or cellres=="fake" or lapres=="fake"):
				final_result="fake"
				print(final_result)
			csv_path = os.path.join(execution_path,'output','final.csv')
			with open(csv_path,'a') as csvfile:
				fieldnames=['images','box','name','lapres','cellres','tvres','final_result']
				writer=csv.DictWriter(csvfile,fieldnames=fieldnames)
				writer.writerow({'images':images,'name':eachObject["name"],'box':eachObject["box_points"],'lapres':lapres,'cellres':cellres,'tvres':tvres,'final_result':final_result})
		except:
				pass

new_csv_path = os.path.join(execution_path,'output','final_temp.csv')
cmd = "mv " + csv_path + " " + new_csv_path
status, output = subprocess.getstatusoutput(cmd)

for mfile in os.listdir(temp_folder_path):
	mfile = os.path.join(temp_folder_path,mfile)
	print("Deleting File - ",mfile)
	os.unlink(mfile)

print("Output File Path = ",new_csv_path)

# finish_time=time.time()
# print(finish_time-detect_time)
# print("successful")