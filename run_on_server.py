import argparse
import subprocess
import pandas as pd
import wget
import os
ap=argparse.ArgumentParser()
ap.add_argument("-o","--object_detection",type=str,default="",help="Run Object Detection Code")
args=vars(ap.parse_args())

print(args["object_detection"])

error_code = input("Enter error_code for which you want to run this script:\n")
input_file = input("Give CSV File Path:\n")
# input_file = "/Users/Hareesh/Workspace/test_projects/model_training_gce/obj_test.csv"
# reading csv file
df = pd.read_csv(input_file)
cwd = os.getcwd()
directory = error_code
if not os.path.exists(directory):
    os.makedirs(directory)

for i, row in df.iterrows():
	# print(row["Response_Code_manual"])
	if row["Response_Code_manual"] == int(error_code):
		try:
			image_url = row["Auth_Image"]
			image_name = image_url.split('/')[-2]+"_"+image_url.split('/')[-1]
			image_download_path = os.path.join(cwd,directory,image_name)
			x = wget.download(image_url,image_download_path)
		except:
			pass


print("Generating CSV.......")

cmd = "python3 object.py --folder " + directory
status, output = subprocess.getstatusoutput(cmd)
print(output.split('\n')[-1])
for mfile in os.listdir(directory):
	mfile = os.path.join(directory,mfile)
	print("Deleting Files - ",mfile)
	os.unlink(mfile)

# apt update && apt install -y libsm6 libxext6